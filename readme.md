# Practice of shell programming 

This folder contains example shell scripts, guided with excellence by the book (see the reference). 

The complete example is in ```/src/htmlGen.sh```. 

## Build in variable 

```$LINENO```: Line number in the script 

## Curly brace usage 
```/src/exit_stat.sh```

The use of the curly braces within the error_exit function is an example of parameter expansion. You can surround a variable name with curly braces (as with ```${PROGNAME}```) if you need to be sure it is separated from surrounding text. Some people just put them around every variable out of habit. That usage is simply a style thing. The second use, ```${1:-"Unknown Error"}``` means that if parameter 1 ```($1)``` is undefined, substitute the string "Unknown Error" in its place. Using parameter expansion, it is possible to perform a number of useful string manipulations. You can read more about parameter expansion in the ```bash``` man page under the topic "EXPANSIONS".

## Notes 

```if ls /etc/*release 1>/dev/null 2>&1;``` 
  
This is understood as disregard the output of ```ls /etc/*release``` by throwing it into the void bin ```/dev/null``` and then capture the exit status at position `2`, which will be true if the output is not empty. See ```/src/htmlGen.sh``` and also in [linux command](http://linuxcommand.org/lc3_wss0130.php).

The three file descriptors are 
* stdin(0)
* stdout(1)
* stderr(2)

## Continuous improvement

I will continue to add more new things that I encountered in this document. 

## Reference 
[Linux Command Guide]([www.](http://linuxcommand.org/))

## Index 

### $
```$@```: all commandline arguments. ```/src/count_cli_arg.sh```

```$$```: process id

```$RANDOM```: A randomly generated number q

### & 

```&&```: Example: ```command1 && command2```. command2 is executed if, and only if, command1 returns an exit status of zero.

### |
```||```: Example: ```command1 || command2```. command2 is executed if, and only if, command1 returns an exit status of none-zero.

### B
```basename```: basename will retrieve the last name from a pathname ignoring any trailing slashes ```/src/dir_cmp.sh```

### D
```du```: short for 'disk usage'. It is a useful command to check the size of files and folders. The default unit is disk block. option ```-h``` is most commonly used to show the space in a 'human readable format'. 

### I 
```id```: id command will give a full description of the current user and the associated characteristics. ```-u``` option will spit the uid value. It is important to know uid is 0 for root access. 

### P 

```pr```: print to a file 

```printf```: This command is similar to c, with which you can format the text accordingly. 



### T

```trap```: The trap command allows you to execute a command when a signal is received by your script. It works like this:

```bash
trap arg signals 
```

An example is shown in ```/src/trapDemo.sh```

```tee```: This commend is like a tee connector. It redirects the result to both a file and the screen at the same time. 

### U
```uname```: Print system information. ```/src/htmlgen.sh```. [(web ref)](http://linuxcommand.org/lc3_man_pages/uname1.html)

### X
```xdd```: Hexdump of binary file or reverse hexdump into binary file. It is good to be used to look at the binary file, deciphering the direct memory dump as firmware to the embedded system, to identify every operation. 

Eg. 
``` bash 
# in /usr/bin
xxd -c 4 zmore | head -n4
```

The above block will read the first four lines of 